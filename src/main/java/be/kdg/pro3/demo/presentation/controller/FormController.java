package be.kdg.pro3.demo.presentation.controller;

import be.kdg.pro3.demo.domain.Person;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

@Controller
public class FormController {
	public static Logger log = LoggerFactory.getLogger(FormController.class);

	@GetMapping("/personForm")
	public String getPage( Model model){
		Person person = new Person("","");
		model.addAttribute(person);
		return "personForm";
	}
	@PostMapping("/personForm")
	public String processPage(Person person, Model model){
		log.info("Received " + person);
		model.addAttribute("person", person);
		return "personForm";
	}
}
